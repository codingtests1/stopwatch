import React from 'react';
import { ReactComponent as Logo } from './logo.svg';
// import { Counter } from './features/counter/Counter';
import StopWatch from './features/stopwatch/StopWatch';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <Logo width={100} height={50} />
        {/*<img src={logo}  viewBox="0 0 60 55" className="App-logo" alt="logo"  height="100" />*/}

        <h1>Stopwatch</h1>
        <StopWatch />
        {/*<Counter />*/}
        {/*<p>*/}
        {/*  Edit <code>src/App.js</code> and save to reload.*/}
        {/*</p>*/}
        {/*<span>*/}
        {/*  <span>Learn </span>*/}
        {/*  <a*/}
        {/*    className="App-link"*/}
        {/*    href="https://reactjs.org/"*/}
        {/*    target="_blank"*/}
        {/*    rel="noopener noreferrer"*/}
        {/*  >*/}
        {/*    React*/}
        {/*  </a>*/}
        {/*  <span>, </span>*/}
        {/*  <a*/}
        {/*    className="App-link"*/}
        {/*    href="https://redux.js.org/"*/}
        {/*    target="_blank"*/}
        {/*    rel="noopener noreferrer"*/}
        {/*  >*/}
        {/*    Redux*/}
        {/*  </a>*/}
        {/*  <span>, </span>*/}
        {/*  <a*/}
        {/*    className="App-link"*/}
        {/*    href="https://redux-toolkit.js.org/"*/}
        {/*    target="_blank"*/}
        {/*    rel="noopener noreferrer"*/}
        {/*  >*/}
        {/*    Redux Toolkit*/}
        {/*  </a>*/}
        {/*  ,<span> and </span>*/}
        {/*  <a*/}
        {/*    className="App-link"*/}
        {/*    href="https://react-redux.js.org/"*/}
        {/*    target="_blank"*/}
        {/*    rel="noopener noreferrer"*/}
        {/*  >*/}
        {/*    React Redux*/}
        {/*  </a>*/}
        {/*</span>*/}
      </header>
    </div>
  );
}

export default App;
