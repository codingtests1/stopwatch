function secondsToTime(secs) {

  const hours = Math.floor(secs / (60 * 60));

  const divisor_for_minutes = secs % (60 * 60);
  const minutes = Math.floor(divisor_for_minutes / 60);

  const divisor_for_seconds = divisor_for_minutes % 60;
  const seconds = Math.ceil(divisor_for_seconds);

  // guesswork!
  const divisor_for_hund = divisor_for_seconds / 100;
  const hund = divisor_for_hund;

  const obj = {
    "tc": pad(hours, 2) + ':' + pad(minutes, 2) + ':' + pad(seconds, 2)+ ':' + pad(hund, 3),
    "h": pad(hours, 2),
    "m": pad(minutes, 2),
    "s": pad(seconds, 2)
  };
  return obj;
}
