import React, { useState, useEffect } from 'react';

// this could be a helpers module or a library
// code copied from SO
const msToTime = (s) => {
  // Pad to 2 or 3 digits, default is 2
  const pad = (n, z = 2) => ('00' + n).slice(-z);
  return pad((s%3.6e6)/6e4 | 0) + ':' + pad((s%6e4)/1000|0) + '.' + pad(s%1000, 2);
};

const getCurrentTime = () => Math.floor(performance.now());

const StopWatch = () => {
  const [ isRolling, setIsRolling ] = useState(false);
  // current timestamp compared to the startTime
  const [ currentTime, setCurrentTime ] = useState(0);
  // used for managing interval
  const [intervalId, setIntervalId] = useState(0);
  const [ startTime, setStartTime ] = useState(0);

  const toggleWatch = (toggle) => {
    if (intervalId && !toggle) {
      clearInterval(intervalId);
      setIntervalId(0);
      return;
    }

    const newIntervalId = setInterval(() => {
      setCurrentTime(getCurrentTime());
      // using 75 as for the user 10 does not provide much visual feedback as it is too fast
      // it is also better for performance reasons
    }, 75);
    setIntervalId(newIntervalId);
  }
  // poor's man componentWillUnmount
  useEffect(() => {
    // clear interval in case it is still running to prevent memory leaks
    return () => {
      if(intervalId) {
        clearInterval(intervalId);
      }
    }
    /* eslint-disable-next-line */
  }, []);

  const updateCmpState = () => {
    // clock started, stop it
    if(isRolling && currentTime > 0) {
      setCurrentTime(getCurrentTime());
      setIsRolling(!isRolling);
      toggleWatch(false);
    }
    // clocked stopped, timer needs to be reset
    else if(!isRolling && currentTime > 0) {
      setCurrentTime(0);
      setStartTime(0);
    }
    // clock started, cache current timestamp and start clock
    else {
      setStartTime(getCurrentTime());
      // use this to prevent a negative value for the first value
      // until the first set interval is executed
      setCurrentTime(getCurrentTime());
      setIsRolling(!isRolling);
      toggleWatch(true);
    }
  }

  return (
    <>
      <div>{msToTime(currentTime - startTime)}</div>
      <button onClick={updateCmpState}>{isRolling ? 'Stop' : 'Start'}</button>
    </>
  );
};

export default StopWatch;
