# Stopwatch coding test

## Short overview
I haven't being coding in a production environment in a while so doing this I felt a bit rusty, therefore not all things might have been done as straightforward as possible.
Due to time constraints no tests had been added.

### Some architectural and code choices
I chose to use ```window.performance``` for time measurements for fun.
The boilerplate was provided by create react app with redux baked in. I usually use redux in most of my apps due to the fact that I think that MVC is the best pattern to follow for all nontrivial apps.
For this particular case I realised redux is a bad idea, however I did not want to restart everything from scratch.
I did not change the default folder structure as it did not feel necessary at this point.

### Final notes
I would have liked to play around with XState and Rx.JS for this particular coding challenge, but as I did not use Rx.JS in 3 years and I never used XState, I felt it would take a lot more than 30 minutes to make it work.
There is an alternative branch that implements this with Rx and Xstate just for fun.

To run
```npm i && yarn start```
The default readme.md bellow is from the create react app boilerplate.

# Getting Started with Create React App and Redux

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app), using the [Redux](https://redux.js.org/) and [Redux Toolkit](https://redux-toolkit.js.org/) template.

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `yarn eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).
